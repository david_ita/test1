package {'apache2':
  ensure => present,
}

service {'apache2':
  ensure => running,
  enable => true,
  require => Package['apache2'],
  subscribe => File['/etc/php.ini'],
}

package {'php':
  ensure => present,  
  before => File['/etc/php.ini'],
}


file { '/var/www/html/index.html':
	ensure => , "purged",
}

file { '/var/www/html/index.php':
        ensure  => file,
        content  => "<h1><center>Dave test</center></h1><?php echo gethostname(); ?>"
}
